module.exports = function(numbers) {
	var isNumberArray = require('validate.io-number-primitive-array');
	if(isNumberArray(numbers)) {
		for(var j = 0; j < numbers.length; j++) {
			for(var i = 1; i < (numbers.length-j); i++) {
				if(numbers[i-1] > numbers[i]) {
					var temp = numbers[i];
					numbers[i] = numbers[i-1];
					numbers[i-1] = temp;
				}
			}
		}
		return numbers;
	}
	else {
		return false;
	}
}